import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export interface IToDoList {
  id: number;
  name: string;
  toDoItems: any[];
}

@Injectable({
  providedIn: 'root'
})

export class TodoApiService {

  readonly todoAPIUrl = "https://localhost:7112/api"

  //Передаем инстанс http слиента в конструктор сервиса
  constructor(private http:HttpClient) { }


  //Методы для взаимодействия со списками задач
  getToDoLists():Observable<any[]> {
    return this.http.get<any>(this.todoAPIUrl + '/ToDoLists');
  }

  getToDoList(id:number):Observable<any> {
    return this.http.get<any>(this.todoAPIUrl + `/ToDoLists/${id}`);
  }

  addToDoList(data:any) {
    return this.http.post(this.todoAPIUrl + '/ToDoLists', data);
  }

  updateToDoList(id:number, data:any) {
    return this.http.put(this.todoAPIUrl + `/ToDoLists/${id}`, data);
  }

  deleteToDoList(id:number) {
    return this.http.delete(this.todoAPIUrl + `/ToDoLists/${id}`);
  }


  //Методы для взаимодействия с элементами списков задач
  getToDoItems():Observable<any[]> {
    return this.http.get<any>(this.todoAPIUrl + '/toDoItems');
  }

  getToDoItem(id:number):Observable<any> {
    return this.http.get<any>(this.todoAPIUrl + `/toDoItems/${id}`);
  }

  addToDoItem(data:any) {
    return this.http.post(this.todoAPIUrl + '/toDoItems', data);
  }

  updateToDoItem(id:number, data:any) {
    return this.http.put(this.todoAPIUrl + `/toDoItems/${id}`, data);
  }

  deleteToDoItem(id:number) {
    return this.http.delete(this.todoAPIUrl + `/toDoItems/${id}`);
  }

}
