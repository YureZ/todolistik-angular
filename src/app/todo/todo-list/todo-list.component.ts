import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';

import { TodoApiService } from 'src/app/todo-api.service';


export interface IToDoList {
  id: number;
  name: string;
  toDoItems: any[];
}


@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})

export class TodoListComponent implements OnInit {

  @Input() viewToDoLists$!:Observable<any[]>

  @Output() onSelectingToDoList = new EventEmitter<Observable<any>>();

  //Поле для хранения выбранного списка дел
  selectToDoList!:Observable<any>;

  //Поле для привязки вводимого названия нового списка задач
  newToDoListTitle : string = "";
  //Поле для создания нового списка дел


  constructor(private service:TodoApiService) { }

  //Метод выбора todolista из общего списка
  selectingToDoList(selToDo:Observable<any>) {
    //Назначем свойство
    this.selectToDoList = selToDo;
    //Запускаем событие к родительскому компоненту с выбранным списком
    this.onSelectingToDoList.emit(selToDo);
  }

  //Метод создания новго списка дел
  addToDoList() {
  //Описываем переменную (названия совпадают с полями API)
    var newToDoList:IToDoList = {
      id: 0,
      name: this.newToDoListTitle,
      toDoItems: []
    }

    //Записываем имя нового списка дел
    //this.newToDoList.name = this.newToDoListTitle;

    //Вызываем метод добавления из сервиса (постим в api)
    this.service.addToDoList(newToDoList);

    //Очищаем поле для ввода нового имени
    this.newToDoListTitle = "";
  }

  deleteToDoList(id:number) {
    this.service.deleteToDoList(id);
  }

  ngOnInit(): void {
  }

}
