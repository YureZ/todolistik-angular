import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-todo-items',
  templateUrl: './todo-items.component.html',
  styleUrls: ['./todo-items.component.css']
})
export class TodoItemsComponent implements OnInit {

  //Поле для списка дел, содержание которого отображается
  @Input() viewToDoList!:Observable<any>;

  constructor() { }

  ngOnInit(): void {
  }

}
