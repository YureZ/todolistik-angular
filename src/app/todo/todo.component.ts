import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TodoApiService } from '../todo-api.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  //Поле для данных о всех списках дел
  toDoLists$!:Observable<any[]>;

  selectedToDoList!:Observable<any>;

  constructor(private service:TodoApiService) { }

  onSelectedToDoList(todo:Observable<any>) {
    this.selectedToDoList = todo;
  }

  ngOnInit(): void {
    this.toDoLists$ = this.service.getToDoLists();
  }

}
