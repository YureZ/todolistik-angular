declare module ToDoModel {

    export interface IToDoList {
        id: number;
        name: string;
        toDoItems: any[];
    }

    export interface IToDoItem {
        id: number;
        done: boolean;
        name: string;
        toDoListId: number;
        toDoList: any;
    }

}