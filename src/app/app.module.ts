//Импортируем модули для взаимодействия с сервером и для форм
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

//Импорт компонентов произошел автоматически при их добавлении через ng
import { AppComponent } from './app.component';
import { TodoComponent } from './todo/todo.component';
import { TodoListComponent } from './todo/todo-list/todo-list.component';
import { TodoItemsComponent } from './todo/todo-items/todo-items.component';

//import { ToDoModel } from './model/ToDo';

//Импортируем сервис для связи с сервером
import { TodoApiService } from './todo-api.service';


@NgModule({
  declarations: [
    //Определения компонентов (тоже добавились автоматом)
    AppComponent,
    TodoComponent,
    TodoListComponent,
    TodoItemsComponent
  ],
  imports: [
    //Модули которые импортировали надо не забыть вставить в билд
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [TodoApiService], //Провайдеры (сервисы) взаимодействия с сервером определяем здесь
  bootstrap: [AppComponent]
})
export class AppModule { }
